//abstract
abstract class Vehicle {
  var series;
  var bodyTypes;
  var wheel;
  var color;
  var price;
  
  //constructor
  Vehicle(series, bodyTypes, wheel, color, price) {
    this.series = series;
    this.bodyTypes = bodyTypes;
    this.wheel = wheel;
    this.color = color;
    this.price = price;
  }
}

//interfaces
class Moving {
  void moveForward() {}
  void moveBackward() {}
  void introduce() {}
}

//Inheritance
class BMW extends Vehicle implements Moving {
  BMW(super.series, super.bodyTypes, super.wheel, super.color, super.price);
  @override
  void moveBackward() {
    print("Move backwards by BMW series ${series}");
  }

  @override
  void moveForward() {
    print("Move forwards by BMW series ${series}");
  }

  @override
  void introduce() {
    print("Hello. I'm is BMW series $series and body type is $bodyTypes");
  }
}

//Inheritance
class Nissan extends Vehicle implements Moving {
  Nissan(super.series, super.bodyTypes, super.wheel, super.color, super.price);

  @override
  void moveBackward() {
    print("Move backwards by Nissan series ${series}");
  }

  @override
  void moveForward() {
    print("Move forwards by Nissan series ${series}");
  }

  @override
  void introduce() {
    print("Hello. I'm Nissan series $series and body type is $bodyTypes");
  }
}

void main(List<String> arguments) {
  var bmw01 = new BMW("4", "Coupe", 4, "Black Sapphire metallic", 4099000);
  bmw01.introduce();
  bmw01.moveForward();
  bmw01.moveBackward();
  var nissan01 = new Nissan("GT-R", "Coupe", 4, "Storm White", 12200000);
  nissan01.introduce();
  nissan01.moveForward();
  nissan01.moveBackward();
}
